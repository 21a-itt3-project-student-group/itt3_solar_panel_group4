import serial
import time

arduinoData = serial.Serial('com3',115200)  #Creating a object
time.sleep(1)

while (1==1): # While loop forever. 1 always 1
    while (arduinoData.inWaiting() == 0): # Loop until there is data.
        pass
    
    dataPacket = arduinoData.readline()   # Variable that reads the data
    
    dataPacket = str(dataPacket, 'utf-8') # Pick only the needed data
    splitPacket = dataPacket.split(',') # Array splitpacket, will split into 3 strings instead of 1

    #Split the arrays into indexes
    Acal = float(splitPacket[0])  # Accel calc
    Gcal = float(splitPacket[1])  # Gyro calc
    Mcal = float(splitPacket[2])  # Mag calc
    Scal = float(splitPacket[3])  # Sys calc
    Pitch = float(splitPacket[4]) # Pitch calc
    Roll = float(splitPacket[5])  # Roll calc
    Yaw = float(splitPacket[6])   # Yaw calc
   
    print("Acal=", Acal, "Gcal=", Gcal, "Mcal=", Mcal, "Scal=", Scal, "Pitch=", Pitch, "Roll=", Roll, "Yaw=", Yaw)

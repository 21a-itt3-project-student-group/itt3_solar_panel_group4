#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>
#include <math.h>
float thetaM; // theta measurement
float phiM; // phi measurement
float thetaFold=0;
float thetaFnew; // theta F new value
float phiFold=0;
float phiFnew;  // phi F new value

float thetaG=0; 
float phiG=0;   

float theta; // Overall system thetha from filter - Pitch
float phi;   // Overall system phi from filter - Roll
float thetaRad;
float phiRad;

float Xm;
float Ym;
float psi;


float dt; // change in time from last measurement
unsigned long millisOld; // Marker of where the clock was last time around. This is a placemarker where we can hold that timer, to use it the next time around

#define BNO055_SAMPLERATE_DELAY_MS (100)// Defines a varible for sample every 100 ms

Adafruit_BNO055 myIMU = Adafruit_BNO055(); // Uses the libary "Adafruit_BNO055" to create an object called myIMU

void setup() {
  // put your setup code here, to run once:
Serial.begin(115200);  // speed of data
myIMU.begin(); //Turns the sensor on by using the libary from Adafruit_BNO055
delay(1000); // Takes 1 sec to start 1000 ms = 1 s
int8_t temp=myIMU.getTemp(); // Makes a temperature measurement |   int8_t = store a number -120 to 120. Takes the temp and place it in the varible temp
myIMU.setExtCrystalUse(true); // Will not use the crystal on the chip but the crystal on the board
millisOld=millis(); //
}

void loop() {
  // put your main code here, to run repeatedly:
uint8_t system, gyro, accel, mg = 0; // Makes 4 varibles ( system, gyro, accel, mg) and set the value to 0 )
myIMU.getCalibration(&system, &gyro, &accel, &mg); // Start calabratin, which sends back the overall system, gyro, accel and mag cal.
imu::Vector<3> acc =myIMU.getVector(Adafruit_BNO055::VECTOR_ACCELEROMETER); // Created a varible called acc where it will use data from the libary BNO055 (ACCELEROMETER)
imu::Vector<3> gyr =myIMU.getVector(Adafruit_BNO055::VECTOR_GYROSCOPE); // Created a varible called gyro where it will use data from the libary BNO055 (GYROSCOPE)
imu::Vector<3> mag =myIMU.getVector(Adafruit_BNO055::VECTOR_MAGNETOMETER); // Created a varible called mag where it will use data from the libary BNO055 (MAGNETOMETER)


  // theta is divided with the half of a circle. The circle is 2*pi*360 (360 degrese in a circle)
  // -atan2 is set so if the "front" of the board is pointing up the value in increase and wise versa
  // By redo the math(SOH CAH TOA), but use y and z instead of x and z the following can be written:
  // Ø = tan^-1(ay/az)

thetaM=-atan2(acc.x()/9.8,acc.z()/9.8)/2/3.141592654*360; // Calculates the tilt (acc) data  (atan2 = inverse tangent = Ø = tan^-1(ax/az)) acc.x will be divided with acc.z Dividing by 9.8 will normalize it to a vector of one
phiM=-atan2(acc.y()/9.8,acc.z()/9.8)/2/3.141592654*360; // Roll from the acc
phiFnew=.95*phiFold+.05*phiM; // Created a variable which will calculate the new value. If the .x values is increased the filter will filter more.
thetaFnew=.95*thetaFold+.05*thetaM; // Created a variable which will calculate the new value(measurement) If the .x values is increased the filter will filter more

dt=(millis()-millisOld)/1000.;  // Present millisecounds - the old millisecounds where it was before. Everytime through the loop it will calculate the new dt
// divided by 1000 to convert to sec.
millisOld=millis();  // Graps existing millis as old to use it the next time around. Will measure in increment of time dt between 1 measurement in the next
                     // It will grap where the system clock is now so it can subtract it in the next time around

theta = (theta+gyr.y()*dt)*.95+thetaM*.05; // Comlimentary filter
phi = (phi-gyr.x()*dt)*.95+phiM*.05;       // Comlimentary filter
thetaG=thetaG+gyr.y()*dt;  // Rotational angle around the y axis
phiG=phiG-gyr.x()*dt;      // Rotational angle around the x axis  (- instead of + to make it the right direction)

phiRad = phi/360*(2*3.14); // Converts degrees to Radians
thetaRad = theta/360*(2*3.14); // -||-

Xm = mag.x()*cos(thetaRad)-mag.y()*sin(phiRad)*sin(thetaRad)+ mag.z()*cos(phiRad)*sin(thetaRad);  // Compensated Compass equation x-magnetometer
Ym = mag.y()* cos(phiRad) + mag.z()*sin(phiRad); // Compensated Compass equation y-magnetometer

psi = atan2 (Ym,Xm)/(2*3.14)*360;  // atan2 (Ym,Xm)/(2*3.14)360; needs to be converted to degrees from radians by doing the following (/2*3.14*360)

Serial.print(accel);
Serial.print(",");
Serial.print(gyro);
Serial.print(",");
Serial.print(mg);
Serial.print(",");
Serial.print(system);
Serial.print(",");
Serial.print(thetaM);  // Measured theta value
Serial.print(",");
Serial.print(phiM);   // Measured phi value
Serial.print(",");
Serial.print(theta);  // Comlimentary filter
Serial.print(",");
Serial.print(phi);  // Comlimentary filter
Serial.print(",");
Serial.println(psi); // Yaw


phiFold=phiFnew;
thetaFold=thetaFnew;

 
delay(BNO055_SAMPLERATE_DELAY_MS);
}

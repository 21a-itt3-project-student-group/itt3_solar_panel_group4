
import RPi.GPIO as GPIO
import time
from ADCDevice import *

ledPin = 11 # define ledpin
adc = ADCDevice() # Define an ADCDevice class object

def setup():
    global adc
    if(adc.detectI2C(0x4b)): # Detect the ads7830.
        adc = ADS7830()
    
    else:
        print("No correct I2C address found, \n"
        "Please use command 'i2cdetect -y 1' to check the I2C address! \n"
        "Program Exit. \n");
        exit(-1)
    global p
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(ledPin,GPIO.OUT)   # set ledPin to OUTPUT mode
    GPIO.output(ledPin,GPIO.LOW)
    
    p = GPIO.PWM(ledPin,1000) # set PWM Frequence to 1kHz
    p.start(0)
    
def loop():
    while True:
        value1 = adc.analogRead(0)    # read the ADC value of channel 0
        value2 = adc.analogRead(1)
        value3 = adc.analogRead(6)
        value4 = adc.analogRead(7)
        valueAvg = (value1 + value2 + value3 + value4)/4
        p.ChangeDutyCycle(valueAvg*100/255)
        voltage1 = valueAvg / 255.0 * 3.3
        voltage2 = valueAvg / 255.0 * 3.3
        voltage3 = valueAvg / 255.0 * 3.3
        voltage4 = valueAvg / 255.0 * 3.3
        voltageAvg = (voltage1 + voltage2 + voltage3 + voltage4)/4
        print ('ADC Value : %d, Voltage : %.2f'%(valueAvg,voltageAvg))
        time.sleep(0.08)

def destroy():
    adc.close()
    p.stop()  # stop PWM
    GPIO.cleanup()
    
if __name__ == '__main__':   # Program entrance
    print ('Program is starting ... ')
    setup()
    try:
        loop()
    except KeyboardInterrupt:  # Press ctrl-c to end the program.
        destroy()
        
    

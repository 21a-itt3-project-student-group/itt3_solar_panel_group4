import serial
import time
import RPi.GPIO as GPIO
import time
from ADCDevice import *


class MAIN:

        # GPIO PINS NEEDS TO BE CHANGED ACCORDING THE SCHEMATIC!!!
    def __init__(self, comport="com3", baudrate = 115200,motoRPin1 = 11, motoRPin2 = 13, enablePin = 15,
                 motoRPin3 = 29, motoRPin4 = 31):
        # Serial connection


        self.value1 = self.analogRead()  # read ADC value of channel 5
        self.comport = comport
        self.baudrate = baudrate

        # ADC setup
        adc = ADCDevice() # ADC Device


        # Analog values
        self.analogRead = ADS7830.analogRead

        # LDR 1
        self.ldr1_val = self.analogRead()
        self.vol1 = float(self.ldr1_val / 255.0 * 3.3)

        # LDR 2
        self.ldr2_val = self.analogRead()
        self.vol2 = float(self.ldr2_val / 255.0 * 3.3)

        # LDR 3
        self.ldr3_val = self.analogRead()
        self.vol3 = float(self.ldr3_val / 255.0 * 3.3)

        # LDR 4
        self.ldr4_val = self.analogRead()
        self.vol4 = float(self.ldr4_val / 255.0 * 3.3)



        # Motor 1
        self.motoRPin1 = motoRPin1
        self.motoRPin2 = motoRPin2

        # Motor 2
        self.motoRPin3 = motoRPin3
        self.motoRPin4 = motoRPin4

        # Motor power
        self.enablePin = enablePin

        # Motor PWM
        self.p = GPIO.PWM(self.enablePin,255)  # create PWM and set Frequence to 1KHz

        # Motors GPIO setup

        self.moveRight = GPIO.setup(motoRPin4, GPIO.OUT)  # RIGHT
        self.moveLeft = GPIO.setup(motoRPin3, GPIO.OUT)  # LEFT
        self.GPIO_setup = GPIO.setup(enablePin, GPIO.OUT)  # Power pin
        self.moveDown = GPIO.setup(motoRPin2, GPIO.OUT)  # DOWN
        self.moveUP = GPIO.setup(motoRPin1, GPIO.OUT)  # UP
        self.GPIO_setmode = GPIO.setmode(GPIO.BOARD)



    def analogRead(self):
        self.analogRead('')
        return self.analogRead

    def mapNUM(self, value, fromLow, fromHigh, toLow, toHigh):
        return (toHigh - toLow) * (value - fromLow) / (fromHigh - fromLow) + toLow

    def ldr1(self):
        while True:
            self.ldr1_val = self.analogRead(0)
            self.vol1 = self.ldr1_val / 255.0 * 3.3
            return self.vol1

    def ldr2(self):
        while True:
            self.ldr2_val = self.analogRead(1)
            self.vol2 = self.ldr2_val / 255.0 * 3.3
            return self.vol2

    def ldr3(self):
        while True:
            self.ldr3_val = self.analogRead(2)
            self.vol3 = self.ldr3_val / 255.0 * 3.3
            return self.vol3

    def ldr4(self):
        while True:
            self.ldr4_val = self.analogRead(3)
            self.vol4 = self.ldr4_val / 255.0 * 3.3
            return self.vol4

    def ldr_compare(self):
        return

    def motor_move(self):
        global p

        self.GPIO_setmode()
        self.moveUP()       # Move up
        self.moveDown()     # Move down
        self.GPIO_setup1()  # NOT SURE ABOUT THIS!!
        self.moveLeft()     # Move left
        self.moveRight()    # Move right
        self.GPIO_setup2()  # NOT SURE ABOUT THIS!!

        self.p = GPIO.PWM(self.enablePin1, 255)  # create PWM and set Frequence to 1KHz

        self.p.start(0), self.p2.start(0)          # Duty cycle = 0
        return self.p.start(0)

    def motor1(self , ADC):
        self.value1 = ADC - 128
        if (self.value1 > 0):  # make motor turn forward
            GPIO.output(self.motoRPin1, GPIO.HIGH)  # motoRPin1 output HIHG level
            GPIO.output(self.motoRPin2, GPIO.LOW)  # motoRPin2 output LOW level
            print('Turn Forward...')
        elif (self.value1 < 0):  # make motor turn backward
            GPIO.output(self.motoRPin1, GPIO.LOW)
            GPIO.output(self.motoRPin2, GPIO.HIGH)
            print('Turn Backward...')
        else:
            GPIO.output(self.motoRPin1, GPIO.LOW)
            GPIO.output(self.motoRPin2, GPIO.LOW)
            print('Motor Stop...')
        self.p.start(self.mapNUM(abs(self.value1), 0, 128, 0, 100))
        return self.p.start(self.mapNUM(abs(self.value1), 0, 128, 0, 100))

        #print('The PWM duty cycle is %d%%\n' % (abs(self.value1) * 100 / 127))  # print PMW duty cycle.

    def motor2(self , ADC):
        self.value2 = ADC - 128
        if (self.value2 > 0):  # make motor turn forward
            GPIO.output(self.motoRPin1, GPIO.HIGH)  # motoRPin1 output HIHG level
            GPIO.output(self.motoRPin2, GPIO.LOW)  # motoRPin2 output LOW level
            print('Turn Forward...')
        elif (self.value2 < 0):  # make motor turn backward
            GPIO.output(self.motoRPin1, GPIO.LOW)
            GPIO.output(self.motoRPin2, GPIO.HIGH)
            print('Turn Backward...')
        else:
            GPIO.output(self.motoRPin1, GPIO.LOW)
            GPIO.output(self.motoRPin2, GPIO.LOW)
            print('Motor Stop...')
        self.p.start(self.mapNUM(abs(self.value2), 0, 128, 0, 100))
        print('The PWM duty cycle is %d%%\n' % (abs(self.value2) * 100 / 127))  # print PMW duty cycle.
        return self.p.start(self.mapNUM(abs(self.value2), 0, 128, 0, 100))




    def serial_data(self):

        self.arduinoData = serial.Serial(self.comport, self.baudrate)
        time.sleep(1)

        while (1 == 1):
            while (self.arduinoData.inWaiting() == 0):
                pass

            # Variable that reads the data
            self.dataPacket = self.arduinoData.readline()

            # Converts the data to a string by using utf-8
            self.dataPacket = str(self.dataPacket, 'utf-8')

            # Array splitpacket that will split the data
            self.splitPacket = self.dataPacket.split(',')

            # Data from Accelererometer calibration

            self.Acal = float(self.splitPacket[0])


            # Data from Gyrcoscopr calibration

            self.Gcal = float(self.splitPacket[1])


            # Prints the values for testing
            print("%-30s %-30s" % ("Accel calibration:", "Gyro calibration:"))
            print("%-30s %-30s" % (float(self.Acal), float(self.Gcal)))

            if self.Acal + self.Gcal == 6:
                return self.Acal, self.Gcal, print(
                    "Calibration succesfully done"), self.arduinoData.close(), self.dataPacket

    def bno_value(self):
        self.arduinoData.open()
        print("Serial connection is open again")
        while (1 == 1):
            while (self.arduinoData.inWaiting() == 0):
                pass

            # Variable that reads the data
            self.dataPacket = self.arduinoData.readline()

            # Converts the data to a string by using utf-8
            self.dataPacket = str(self.dataPacket, 'utf-8')

            # Array splitpacket that will split the data
            self.splitPacket = self.dataPacket.split(',')

            self.Pitch = float((self.splitPacket[6]))
            print("Pitch:", self.Pitch)


            self.Roll = float(self.splitPacket[7])
            print("Roll:", self.Roll)
            if (self.Pitch) >= float('40'):
                print("STOP YOU GO TOO FAR!")
            elif self.Pitch <= float('-40'):
                print("STOP YOU GO TOO FAR!")
            if (self.Roll) >= float('40'):
                print("STOP YOU GO TOO FAR!")
            elif self.Roll <= float('-40'):
                print("STOP YOU GO TOO FAR!")

            #return self.Pitch,self.Roll, self.arduinoData.close()

    def main(self):
        start.serial_data()
        while True:
            start.bno_value()
            start.ldr1(),start.ldr2_val(),start.ldr3(),start.ldr4()
            start.ldr_compare(),start.motor1(), start.motor2()






if __name__ == "__main__":
    try:
        start = MAIN()
        start.main()
    except KeyboardInterrupt:
        exit()




